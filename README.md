# Node10 workspace

Barebones Vagrant project with Node10. Use this for sandboxing etc. Keep it generic

## How to

- Clone
- delete .git folder
- rename stuff.
- in /vagrant/vagrantfile, set v.name to something sane
- in /vagrant/provision.sh, set machine name to the same something sane